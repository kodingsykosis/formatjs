String.prototype.format = function () {
    var results = this.toString(), translate = function (format, key, value) {
        var re = new RegExp('{(?=[^{])' + key + '(?::(?:\\^([^}]+)|\\$([^}]+)|!([^}]+)|([^}]+)))?}(?:(?=[^}])|$)', 'g');
        return format.replace(re, function (match, group1, group2, group3, group4) {
            var str;
            switch(typeof value) {
                case 'undefined':
                    return '';
                case 'number':
                    if (group4 && /[cdefpx#0][,0-9]*/.test(group4)) {
                        str = String.formatNumber(match, value);
                        break;
                    }
                case 'boolean':
                    str = value.toString();
                    break;
                default:
                    if(group4 && new Date(value).toString() !== 'Invalid Date') {
                        if(typeof Date.prototype.format === 'undefined') {
                            throw "Format not defined for type Date.";
                        }
                        str = (new Date(value)).format(group4);
                    } else {
                        str = value || group3 || '';
                    }
            }
            return (group1 || '') + str + (group2 || '');
        });
    }, data = arguments.length === 1 && typeof arguments[0] === 'object' ? arguments[0] : arguments;
    for(var prop in data) {
        if (data[prop] !== null) {
            results = translate(results, prop, data[prop]);
        }
    }
    return results.replace(/{(?=[^{])[^:}]+(?::!([^}]*)|[^}]+)?}(?:(?=[^}])|$)/g, '$1')
        .replace(/\{\{/g, '{')
        .replace(/\}\}/g, '}');
};
String.prototype.containsFormatting = function () {
    return (/{(?=[^}])[^}]+}(?:(?=[^}])|$)/).test(this);
};