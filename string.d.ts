interface String {
    format(...args:any[]):string;
    containsFormatting():boolean;
}